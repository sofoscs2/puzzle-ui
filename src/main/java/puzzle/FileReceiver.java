package puzzle;

import algorithm.UnboundedKnapsack;
import com.google.common.base.Splitter;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload;
import model.CustomerModel;

import java.io.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import static algorithm.UnboundedKnapsack.solve;
import static puzzle.PuzzleUI.responseArea;

/**
 * Implement both receiver that saves upload in a file and
 * listener for successful upload
 */
public class FileReceiver implements Upload.Receiver, Upload.SucceededListener {
    public File file;
    public int monthlyInventory;
    public List<CustomerModel> customerModelList = new ArrayList<CustomerModel>();

    /**
     * Reads input file data in a file and produces OutputStream
     *
     * @param filename
     * @param mimeType
     * @return
     */
    public OutputStream receiveUpload(String filename,
                                      String mimeType) {
        // Create upload stream
        FileOutputStream fos = null; // Stream to write to
        try {
            cleanData();
            // Open the file for writing.
            final String dir = System.getProperty("user.dir");
            file = new File(dir + "\\" + filename);

            fos = new FileOutputStream(file);
        } catch (final java.io.FileNotFoundException e) {
            new Notification("Could not open file<br/>",
                    e.getMessage(),
                    Notification.Type.ERROR_MESSAGE)
                    .show(Page.getCurrent());
            return null;
        }
        return fos; // Return the output stream to write to
    }

    public void uploadSucceeded(Upload.SucceededEvent event) {
        // set the list of customer data
        setCustomerModelList();

        final UnboundedKnapsack.Solution solution = solve(customerModelList, monthlyInventory);
        solution.getUsedCustomerModels().forEach((bi) -> {
//            System.out.println(MessageFormat.format("{0},{1,number,#},{2,number,#},{3,number,#}", bi.getCustomerModel().getCustomer(), bi.getCounter(), bi.getTotalImpressions(), bi.getTotalRevenue()));
            addInputFileResponse(MessageFormat.format("{0},{1,number,#},{2,number,#},{3,number,#}", bi.getCustomerModel().getCustomer(), bi.getCounter(), bi.getTotalImpressions(), bi.getTotalRevenue()) + "\n");
        });
//        System.out.println(MessageFormat.format("{0,number,#},{1,number,#}", solution.getTotalImpressions(), solution.getTotalRevenue()));
        addInputFileResponse(MessageFormat.format("{0,number,#},{1,number,#}", solution.getTotalImpressions(), solution.getTotalRevenue()));
    }

    /**
     * add text to response area if file contains correctly formatted data
     * @param fileData
     */
    private void addInputFileResponse(String fileData) {
        responseArea.setVisible(true);
        if (monthlyInventory != 0 && customerModelList.size() != 0) {
            String areaContext = responseArea.getValue();
            areaContext += fileData;
            responseArea.setValue(areaContext);
        } else {
            responseArea.setValue("Wrong input file");
        }
    }

    /**
     * Reads file data line by line
     * to Customer model list
     * @return
     */
    private void setCustomerModelList() {
        try {
            int lineCounter = 0;
            monthlyInventory = 0;
            BufferedReader br = new BufferedReader(new FileReader(file));
            for (String line; (line = br.readLine()) != null; ) {
                if (lineCounter == 0 && isTextInterger(line)) {
                    monthlyInventory = Integer.parseInt(line);
                } else {
                    List<String> customerData = Splitter.on(',').splitToList(line);
                    if (isValidCustomerRecord(customerData)) {
                        CustomerModel customer = new CustomerModel(customerData);
                        customerModelList.add(customer);
                    }
                }
                lineCounter++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * checks if input string is arithmetic
     * @param text
     * @return
     */
    private boolean isTextInterger(String text) {
        String regex = "\\d+";
        return text.matches(regex);
    }

    /**
     * check if a text can load to customer model
     * @param customerData
     * @return
     */
    private boolean isValidCustomerRecord(List<String> customerData) {
        // 3 values load
        if (customerData.size() != 3) {
            return false;
        }
        // check if values 2,3 are arithmetic
        if (!isTextInterger(customerData.get(1)) || !isTextInterger(customerData.get(2))) {
            return false;
        }
        return true;
    }

    public void cleanData() {
        file = null;
        customerModelList = new ArrayList<>();
        monthlyInventory = 0;
    }
}
