package puzzle;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.Upload.ProgressListener;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.StartedListener;

import javax.servlet.annotation.WebServlet;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class PuzzleUI extends UI {

    // Create the response area container
    public static TextArea responseArea;


    /**
     * Object initialization
     * @param vaadinRequest
     */
    @Override
    protected void init(VaadinRequest vaadinRequest) {
        //response responseArea initial configuration
        responseArea = new TextArea("Uploaded text");
        responseArea.setVisible(false);
        responseArea.setWidth(400, Unit.PIXELS);
        responseArea.setHeight(400, Unit.PIXELS);

        //file receiver
        FileReceiver receiver = new FileReceiver();

        // Create the upload with a caption and set receiver later
        final Upload upload = new Upload("Upload it here", receiver);
        upload.setButtonCaption("Start Upload");
        upload.addSucceededListener(receiver);

        // Prevent too big downloads
        final long UPLOAD_LIMIT = 1000000l;
        upload.addStartedListener(new StartedListener() {
            @Override
            public void uploadStarted(StartedEvent event) {
                responseArea.setValue("");
                if (event.getContentLength() > UPLOAD_LIMIT) {
                    Notification.show("Too big file",
                            Notification.Type.ERROR_MESSAGE);
                    upload.interruptUpload();
                }
            }
        });

        // Check the size also during progress
        upload.addProgressListener(new ProgressListener() {
            @Override
            public void updateProgress(long readBytes, long contentLength) {
                if (readBytes > UPLOAD_LIMIT) {
                    Notification.show("Too big file",
                            Notification.Type.ERROR_MESSAGE);
                    upload.interruptUpload();
                }
            }
        });

        // Put the components in a panel
        Panel panel = new Panel("Ooyala programming puzzle");
        VerticalLayout panelContent = new VerticalLayout();
        panelContent.setMargin(true);
        panelContent.addComponents(upload, responseArea);
        panel.setContent(panelContent);

        setContent(panel);
    }
    private void resetValue() {

    }

    /**
     * Default Vaadin servlet configuration
     */
    @WebServlet(urlPatterns = "/*", name = "PuzzleUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = PuzzleUI.class, productionMode = false)
    public static class PuzzleUIServlet extends VaadinServlet {
    }
}
