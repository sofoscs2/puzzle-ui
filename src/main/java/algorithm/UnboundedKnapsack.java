package algorithm;

import model.CustomerModel;
import model.UsedCustomerModels;

import java.math.BigInteger;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;


/**
 * This is an implementation of unbounded knapsack algorithm
 *
 * To save memory space first calculate and apply to customer data and
 * to monthlyInventory a normalization factor based on greatest common divisor
 *
 * Then apply a dynamic programming knapsack algorithmic solution
 *
 * Finally de-normalize values
 *
 * The described approach is availabe in https://gist.github.com/sbilinski/9acdc05a9ad17c95eabf
 */
public final class UnboundedKnapsack {

    private UnboundedKnapsack() {}

    /**
     * Knapsack problem solution algorithm.
     */
    public static final class Solution {

        public final Collection<UsedCustomerModels> usedCustomerModels;

        Solution(Collection<UsedCustomerModels> usedCustomerModels) {
            this.usedCustomerModels = usedCustomerModels;
        }

        public Collection<UsedCustomerModels> getUsedCustomerModels() {
            return usedCustomerModels;
        }

        public Integer getTotalRevenue() {
            return usedCustomerModels.stream().map(UsedCustomerModels::getTotalRevenue).reduce(0, Integer::sum);
        }

        public Integer getTotalImpressions() {
            return usedCustomerModels.stream().map(UsedCustomerModels::getTotalImpressions).reduce(0, Integer::sum);
        }

        public Integer getTotalUsedCustomerModels() {
            return usedCustomerModels.stream().map(UsedCustomerModels::getCounter).reduce(0, Integer::sum);
        }

    }

    /**
     * Solves the unbounded knapsack problem for a given collection of items.
     *
     * In this case for customer models in a monthly inventory
     *
     * @param customerModelList available items
     * @param inventory knapsack capacity (total)
     */
    public static Solution solve(final List<CustomerModel> customerModelList, final Integer inventory) {
        final Integer normFactor = calculateNormalizationFactor(customerModelList, inventory);
        final List<CustomerModel> normalizedCustomerModelList = normalizedItemsCopy(customerModelList, normFactor);
        final Integer normalizedInventory = inventory / normFactor;

        final Solution solution = dynamicProgrammingSolution(normalizedCustomerModelList, normalizedInventory);

        return denormalizedSolution(solution, normFactor);
    }

    /**
     * Dynamic programming implementation based on <a href="http://rosettacode.org/wiki/Knapsack_problem/Unbounded/Python_dynamic_programming#DP.2C_single_size_dimension">Rosetta Code</a>.
     */
    private static Solution dynamicProgrammingSolution(final List<CustomerModel> customerModelList, final Integer inventory) {
        Collections.sort(customerModelList, (CustomerModel i1, CustomerModel i2) ->
                (i2.getProfitRatio().compareTo(i1.getProfitRatio())));

        //Keeps track of current sack value for given capacity
        final int[] usedRevenue = new int[inventory+1];

        //Keeps track of items that "completed" a given capacity. That is, lastItem[c] is the index of the item, that was
        //most recently added to the sack with capacity 'c'
        final int[] lastCustomerModel = new int[inventory+1];

        //There is no "last item" in a given capacity by default. We'll be using a negative index to distinguish this
        //case (clarity).
        Arrays.fill(lastCustomerModel, -1);

        for (int i=0; i<customerModelList.size(); i++) {
            final CustomerModel customerModel = customerModelList.get(i);
            final int impressionsPerCampaign = customerModel.getImpressionsPerCampaign();
            final int revenuePerCampaign = customerModel.getRevenuePerCampaign();

            for (int c=impressionsPerCampaign; c <= inventory; c++) {
                final int trial = usedRevenue[c-impressionsPerCampaign] + revenuePerCampaign;

                if (usedRevenue[c] < trial) {
                    usedRevenue[c] = trial;
                    lastCustomerModel[c] = i;
                }
            }
        }

        final List<UsedCustomerModels> usedCustomerModelsList = new ArrayList<>();
        final int[] counters = collectCustomerModelCounters(customerModelList, inventory, lastCustomerModel);
        for (int i=0; i < customerModelList.size(); i++) {
            usedCustomerModelsList.add(new UsedCustomerModels(customerModelList.get(i), counters[i]));
        }
        return new Solution(Collections.unmodifiableCollection(usedCustomerModelsList));
    }

    /**
     * A helper method for the dynamic programming solution, which maps the lastItem array to an array of counters.
     */
    private static int[] collectCustomerModelCounters(final List<CustomerModel> customerModelList, final Integer inventory, final int[] lastCustomerModel) {
        int counters[] = new int[customerModelList.size()];
        int c = inventory;
        while (c > 0) {
            int customerModelIndex = lastCustomerModel[c];

            //If no item was added in this capacity, move to the previous one.
            while (customerModelIndex < 0 && c > 0) {
                c--;
                customerModelIndex = lastCustomerModel[c];
            }

            //If an item was found, increment it's counter and move "down" by current item size.
            if (customerModelIndex >= 0 && c > 0) {
                counters[customerModelIndex]++;
                c = c - customerModelList.get(customerModelIndex).getImpressionsPerCampaign();
            }
        };
        return counters;
    }

    /**
     * Calculates the normalization factor for input data (to save memory during dp iterations)
     */
    private static Integer calculateNormalizationFactor(final List<CustomerModel> customerModelList, final Integer capacity) {
        final BiFunction<Integer, Integer, Integer> gcd = (Integer a, Integer b) -> BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).intValue();
        return customerModelList.stream().map((customerModel) -> {
            return customerModel.getImpressionsPerCampaign() == 0 ? capacity : gcd.apply(customerModel.getImpressionsPerCampaign(), capacity);
        }).reduce(capacity, Integer::min);
    }

    /**
     * Creates a normalized copy of input customer models.
     */
    private static List<CustomerModel> normalizedItemsCopy(final List<CustomerModel> customerModelList, final int normFactor) {
        final List<CustomerModel> normalizedCustomerModelList = new ArrayList<>(customerModelList.size());
        for (CustomerModel i: customerModelList) {
            normalizedCustomerModelList.add(new CustomerModel(i.getCustomer(), i.getImpressionsPerCampaign() / normFactor, i.getRevenuePerCampaign()));
        }
        return normalizedCustomerModelList;
    }

    /**
     * Creates a de-normalized copy of the solution.
     */
    private static Solution denormalizedSolution(final Solution solution, final Integer normFactor) {
        final List<UsedCustomerModels> denormItems = solution.getUsedCustomerModels().stream().map((usedCustomerModel) -> {
            final CustomerModel customerModel = usedCustomerModel.getCustomerModel();
            return new UsedCustomerModels(new CustomerModel(customerModel.getCustomer(), customerModel.getImpressionsPerCampaign() * normFactor, customerModel.getRevenuePerCampaign() ), usedCustomerModel.getCounter());
        }).collect(Collectors.toList());

        return new Solution(Collections.unmodifiableCollection(denormItems));
    }
}
