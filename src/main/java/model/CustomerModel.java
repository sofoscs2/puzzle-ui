package model;

import java.util.List;

/**
 * Customer model which loads items
 * and calculates profit ratio
 */
public class CustomerModel  {
    private String customer;
    private Integer impressionsPerCampaign;
    private Integer revenuePerCampaign;

    /**
     * Constructor which loads string data from list
     * @param customerData
     */
    public CustomerModel(List<String> customerData) {
        this.customer = customerData.get(0);
        this.impressionsPerCampaign = Integer.parseInt(customerData.get(1));
        this.revenuePerCampaign = Integer.parseInt(customerData.get(2));
    }

    /**
     * Contructor which loads
     * @param customer
     * @param impressionsPerCampaign
     * @param revenuePerCampaign
     */
    public CustomerModel(String customer, int impressionsPerCampaign, int revenuePerCampaign) {
        this.customer = customer;
        this.impressionsPerCampaign = impressionsPerCampaign;
        this.revenuePerCampaign = revenuePerCampaign;
    }

    public String getCustomer() {
        return customer;
    }

    public Integer getImpressionsPerCampaign() {
        return impressionsPerCampaign;
    }

    public Integer getRevenuePerCampaign() {
        return revenuePerCampaign;
    }

    public Double getProfitRatio() {
        double profitRatio = (double) 0;
        if (this.revenuePerCampaign != 0 && this.impressionsPerCampaign != 0) {
            profitRatio = (double) this.revenuePerCampaign / (double) this.impressionsPerCampaign;
        }
        return profitRatio;
    }
}
