package model;

/**
 * Takes care already used customer models and calculates
 * total revenue and impressions
 */
public final class UsedCustomerModels {

        private final CustomerModel customerModel;
        private final Integer counter;

        public UsedCustomerModels(CustomerModel customerModel, Integer counter) {
            this.customerModel = customerModel;
            this.counter = counter;
        }

        public CustomerModel getCustomerModel() {
            return customerModel;
        }

        public Integer getCounter() {
            return counter;
        }

        public Integer getTotalRevenue() {
            return customerModel.getRevenuePerCampaign() * counter;
        }

        public Integer getTotalImpressions() {
            return customerModel.getImpressionsPerCampaign() * counter;
        }
}
